package com.zhanggen.mapper;

import com.zhanggen.domian.Student;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface StudentMapper {
    //查询所有
    @Select("select * from student")
    List<Student> findAll();

    //保存
    @Insert("insert into student values(#{number},#{name},#{birthday},#{address})")
    void save(Student student);

    //编辑时-根据主键查询
    @Select("select * from student where number=#{number}")
    Student findById(String number);

    //编辑时-修改
    @Update("update student set name = #{name},birthday = #{birthday},address = #{address} where number = #{number}")
    void update(Student student);

    //根据注解删除
    @Delete("delete from student where number = #{number}")
    void delete(String number);
}
