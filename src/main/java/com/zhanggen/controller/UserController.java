package com.zhanggen.controller;

import com.zhanggen.domian.Student;
import com.zhanggen.service.StudentService;
import com.zhanggen.vo.ResultInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController//@Controller+@ResponseBody
public class UserController {
    @Autowired //从spring的容器中调用studentService对象
    private StudentService studentService;

    @GetMapping("/student")
    public ResultInfo findAll() {
        List<Student> studentList = studentService.findAll();
        return ResultInfo.success(studentList);
    }
    //保存
    @PostMapping("/student")
    public ResultInfo save(@RequestBody Student student ) {
        studentService.save(student);
        return ResultInfo.success(null);
    }
    //根据主键查询
    @GetMapping("/student/{number}")
    public ResultInfo findById(@PathVariable String number) {
        Student student=studentService.findById(number);
        System.out.println(number);
        return ResultInfo.success(student);
    }
    //根据主键修改
    @PutMapping("/student")
    public ResultInfo update(@RequestBody Student student) {
        //调用service修改
        studentService.update(student);
        //返回结果
        return ResultInfo.success(null);
    }
    //根据主键删除
    @DeleteMapping("/student/{number}")
    public ResultInfo delete(@PathVariable String number) {
        //调用service修改
        studentService.delete(number);
        //返回结果
        return ResultInfo.success(null);
    }


}
