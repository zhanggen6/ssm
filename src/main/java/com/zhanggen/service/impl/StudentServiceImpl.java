package com.zhanggen.service.impl;

import com.zhanggen.domian.Student;
import com.zhanggen.mapper.StudentMapper;
import com.zhanggen.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service //把当前Service层实现类实例化的对象，放入spring的IOC容器
@Transactional
public class StudentServiceImpl implements StudentService {
    @Autowired //调用spring的IOC容器中的StudentMapper对象
    private StudentMapper studentMapper;

    //查询所有
    public List<Student> findAll() {
        List<Student> userList = studentMapper.findAll();
        return userList;
    }

    //保存
    public void save(Student student) {
        studentMapper.save(student);
        //测试声明式事务和springMVC的统一异常处理
        // int i=1/0;
    }

    //根据主键查询
    public Student findById(String number) {
        return studentMapper.findById(number);
    }

    //根据主键修改
    public void update(Student student) {
        studentMapper.update(student);
    }
    //根据主键删除
    public void delete(String number) {
        studentMapper.delete(number);

    }
}
