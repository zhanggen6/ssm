package com.zhanggen.service;

import com.zhanggen.domian.Student;

import java.util.List;

public interface StudentService {
    //查询所有
    List<Student> findAll();

    //新增
    void save(Student student);

    //根据number主键查询
    Student findById(String number);

    //根据number主键修改
    void update(Student student);

    //根据number主键删除
    void delete(String number);
}
