package com.zhanggen.handler;

import com.zhanggen.vo.ResultInfo;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice //该注解可以声明当前类是一个用于统一处理3层异常的类
@ResponseBody
public class CommonExceptionHandler {
    @ExceptionHandler(Exception.class)  //标注该方法可以处理哪些类型的异常
    public ResultInfo handleAllException(Exception e) {
        //记录错误日志
        e.printStackTrace();
        //给前端提示
        return ResultInfo.error("后端异常");
    }
}
