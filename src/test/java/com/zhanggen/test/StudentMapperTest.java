package com.zhanggen.test;

import com.zhanggen.domian.Student;
import com.zhanggen.mapper.StudentMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class StudentMapperTest {
    @Test
    public void testFindAll() throws IOException {
        //1.读取配置mybatis文件为数据流
        InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");
        //2.创建SqlSessionFactory工厂
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        //3.创建sqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //4.获取studentMapper对象执行方法
        StudentMapper studentMapper = sqlSession.getMapper(StudentMapper.class);
        //5.提交事务
        List<Student> studenList = studentMapper.findAll();
        for (Student student : studenList) {
            System.out.println(student);
        }
        //5.释放资源
        sqlSession.commit();
        sqlSession.close();


    }
}
